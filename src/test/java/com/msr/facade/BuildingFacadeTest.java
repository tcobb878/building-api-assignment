package com.msr.facade;

import com.msr.data.SiteDao;
import com.msr.dto.SiteDto;
import com.msr.model.Site;
import com.msr.model.SiteUses;
import com.msr.model.UseTypes;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.management.MalformedObjectNameException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BuildingFacadeTest {

    @Mock
    private SiteDao mockSiteDao;

    private BuildingFacade buildingFacade;

    @Before
    public void setUp(){
        buildingFacade = new BuildingFacade(mockSiteDao);
    }

    /**
     * I just wrote one happy patch unit test here so I could hit a majority of my functions in the facade
     * - If time permitted I'd write test for each function individually along with the getList searches
     */
    @Test
    public void getSideById_SuccessfulTest(){
        when(mockSiteDao.findSiteById(anyInt())).thenReturn(buildSiteReturn());
        SiteDto siteDto = buildingFacade.getSiteById(1);

        assertEquals( "San Diego", siteDto.getCity());
        assertEquals( 13000, siteDto.getTotal_size());
        assertEquals( 54, siteDto.getPrimaryUse().getId());
        assertEquals("707 Broadway Suite 1000", siteDto.getAddress() );
        assertEquals("measurabl", siteDto.getName());

    }


    private Site buildSiteReturn(){
        Site site = new Site();
        site.setId(1);
        site.setName("measurabl");
        site.setAddress("707 Broadway Suite 1000");
        site.setCity("San Diego");
        site.setState("CA");
        site.setZipcode("92101");

        UseTypes useTypesOffice = new UseTypes();
        useTypesOffice.setName("office");
        useTypesOffice.setId(54);

        UseTypes useTypesDataCenter = new UseTypes();
        useTypesDataCenter.setId(4);
        useTypesDataCenter.setName("Data Center");

        List<SiteUses> siteUsesList = new ArrayList<>();

        SiteUses siteUses = new SiteUses();
        siteUses.setId(1);
        siteUses.setSiteId(1);
        siteUses.setSizeSqft(3000);
        siteUses.setDescription("MSR Office - Suite 1");
        siteUses.setUseTypes(useTypesOffice);
        siteUsesList.add(siteUses);

        SiteUses siteUses1 = new SiteUses();
        siteUses1.setId(2);
        siteUses1.setSiteId(1);
        siteUses1.setSizeSqft(3000);
        siteUses1.setDescription("MSR Office - Suite 2");
        siteUses1.setUseTypes(useTypesOffice);
        siteUsesList.add(siteUses1);

        SiteUses siteUses2 = new SiteUses();
        siteUses2.setId(3);
        siteUses2.setSiteId(1);
        siteUses2.setSizeSqft(3000);
        siteUses2.setDescription("MSR Office - Suite 3");
        siteUses2.setUseTypes(useTypesOffice);
        siteUsesList.add(siteUses2);

        SiteUses siteUses3 = new SiteUses();
        siteUses3.setId(4);
        siteUses3.setSiteId(1);
        siteUses3.setSizeSqft(4000);
        siteUses3.setDescription("data center");
        siteUses3.setUseTypes(useTypesDataCenter);
        siteUsesList.add(siteUses3);

        site.setSiteUses(siteUsesList);

        return site;


    }


}
