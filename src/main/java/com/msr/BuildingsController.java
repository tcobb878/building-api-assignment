package com.msr;

import com.msr.dto.SiteDto;
import com.msr.facade.BuildingFacade;
import com.msr.model.Site;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Respond to site requests
 *
 * @author Measurabl
 * @since 2019-06-06
 */
@RestController
@RequestMapping("/buildings")
public class BuildingsController {

    /* Sample Output messages. */
    private final static String SAMPLE_RESPONSE_BASE = "This is a sample response to test if your BuildingController is responding appropriately.  ";
    static final String SAMPLE_PARAM_PROVIDED = SAMPLE_RESPONSE_BASE + "The request param you passed was: ";
    static final String NO_SAMPLE_PARAM_PROVIDED = SAMPLE_RESPONSE_BASE + "No request param was provided.";
    static final String SAMPLE_EXCEPTION_MESSAGE = SAMPLE_RESPONSE_BASE + "An expected error was thrown.";

    private final BuildingFacade buildingFacade;

    @Autowired
    public BuildingsController(BuildingFacade buildingFacade){
        this.buildingFacade = buildingFacade;
    }



    /**
     * Used simply to check if your BuildingsController is responding to requests.
     * Has no function other than echoing.
     * @return A sample message based on the input parameters.
     * @throws Exception Only when 'throwError' is true.
     */
    @ApiOperation("Returns a sample message for baseline controller testing.")
    @GetMapping("/sample")
    public String getSampleResponse(@ApiParam("The message that will be echoed back to the user.")
                                    @RequestParam(required = false) final String message,
                                    @ApiParam("Forces this endpoint to throw a generic error.")
                                    @RequestParam(required = false) final boolean throwError) throws Exception {
        String response;
        if(throwError) {
            throw new Exception(SAMPLE_EXCEPTION_MESSAGE);
        } else if (StringUtils.isEmpty(message)){
            response = NO_SAMPLE_PARAM_PROVIDED;
        } else {
            response = SAMPLE_PARAM_PROVIDED + message;
        }
        return response;
    }

    /**
     * Get a site returned by ID
     * @return SiteDto of the site searched
     */
    @ApiOperation("Returns a sample message for baseline controller testing.")
    @GetMapping("/getSite")
    public SiteDto getSiteById(@ApiParam("Forces this endpoint to throw a generic error.")
                                    @RequestParam(required = true) int siteId) throws Exception {

        SiteDto response;
        response = buildingFacade.getSiteById(siteId);

        return response;
    }

    /**
     * Get a list of sites
     * Has no function other than echoing.
     * @return A list of sites from the h2 database
     */
    @ApiOperation("getListOfSites")
    @GetMapping("/getListOfSites")
    public List<SiteDto> getListOfSites() {

        List<SiteDto> response = new ArrayList<>();
        response = buildingFacade.getListOfSites();
        return response;
    }


    /**
     * Search to get a list of sites by zipCode
     * @return List of sites matching the zipCode
     */
    @ApiOperation("getListOfSitesByZip")
    @GetMapping("/getListOfSitesByZip")
    public List<SiteDto> getListOfSitesByZipCode(@ApiParam("ZipCode of the sites you want to return")
            @RequestParam(required = true) String zipCode) {

        List<SiteDto> response = new ArrayList<>();
        response = buildingFacade.getListOfSitesByZip(zipCode);
        return response;
    }

}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    