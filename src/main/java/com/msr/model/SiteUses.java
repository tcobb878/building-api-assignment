package com.msr.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Transient;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Site uses POJO
 *
 * @author Measurabl
 * @since 2019-06-11
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class SiteUses {
    @Id
    private int id;

    @JsonProperty("site_id")
    private int siteId;

    private String description;

    @JsonProperty( "size_sqft")
    private long sizeSqft;

    @JsonProperty("use_type_id")
    private int useTypeId;

    @Transient
    private UseTypes useTypes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSiteId() {
        return siteId;
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getSizeSqft() {
        return sizeSqft;
    }

    public void setSizeSqft(long sizeSqft) {
        this.sizeSqft = sizeSqft;
    }

    public int getUseTypeId() {
        return useTypeId;
    }

    public void setUseTypeId(int useTypeId) {
        this.useTypeId = useTypeId;
    }

    public UseTypes getUseTypes() {
        return useTypes;
    }

    public void setUseTypes(UseTypes useTypes) {
        this.useTypes = useTypes;
    }
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    