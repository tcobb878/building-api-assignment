package com.msr.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

/**
 * Lookup types
 *
 * @author Measurabl
 * @since 2019-06-11
 */
@Data
@Entity
public class UseTypes {

    @Id
    private int id;

    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UseTypes useTypes = (UseTypes) o;
        return id == useTypes.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    