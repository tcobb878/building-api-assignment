package com.msr;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.msr.data.SiteRepository;
import com.msr.data.SiteUsesRepository;
import com.msr.data.UseTypesRepository;
import com.msr.model.Site;
import com.fasterxml.jackson.core.type.TypeReference;
import com.msr.model.SiteUses;
import com.msr.model.UseTypes;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@SpringBootApplication
@Configuration
@ComponentScan
@EnableAutoConfiguration
@EnableJpaRepositories
@EnableSwagger2
public class BuildingsApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(BuildingsApiApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(SiteRepository siteRepository, UseTypesRepository useTypesRepository, SiteUsesRepository siteUsesRepository) {
		return args -> {
			ObjectMapper mapper = new ObjectMapper();
			TypeReference<List<Site>> typeReferenceSite = new TypeReference<List<Site>>(){};
			TypeReference<List<UseTypes>> typeReferenceUseType = new TypeReference<List<UseTypes>>(){};
			TypeReference<List<SiteUses>> typeReferenceSiteUses = new TypeReference<List<SiteUses>>(){};
			InputStream inputStreamSites = TypeReference.class.getResourceAsStream("/data/sites.json");
			InputStream inputStreamUseTypes = TypeReference.class.getResourceAsStream("/data/use_types.json");
			InputStream inputStreamSiteUses = TypeReference.class.getResourceAsStream("/data/site_uses.json");
			try {
				List<Site> sites = mapper.readValue(inputStreamSites ,typeReferenceSite);
				List<UseTypes> useTypes = mapper.readValue(inputStreamUseTypes ,typeReferenceUseType);
				List<SiteUses> siteUses = mapper.readValue(inputStreamSiteUses ,typeReferenceSiteUses);
				siteRepository.saveAll(sites);
				useTypesRepository.saveAll(useTypes);
				siteUsesRepository.saveAll(siteUses);
				System.out.println("Sites Successfully Saved");
			} catch (IOException e){
				System.out.println("Unable to save Sites: " + e.getMessage());
			}
		};
	}
}
