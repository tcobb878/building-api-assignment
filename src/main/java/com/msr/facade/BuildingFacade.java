package com.msr.facade;

import com.msr.data.SiteDao;
import com.msr.dto.SiteDto;
import com.msr.model.Site;
import com.msr.model.SiteUses;
import com.msr.model.UseTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class BuildingFacade {

    private final SiteDao siteDao;

    @Autowired
    public BuildingFacade (SiteDao siteDao){
        Assert.notNull(siteDao, "SiteDao is null.");
        this.siteDao = siteDao;
    }

    /**
     * find site by id
     *
     * @param id to search for
     * @return site in form of data transfer object that matches the id.
     */
    public SiteDto getSiteById(int id){
        //getSiteFromDB
        SiteDto siteDto = new SiteDto();
        try {
            Site site = siteDao.findSiteById(id);
            siteDto = populateDto(site);
        }catch(Exception ex){
            System.out.println("Error getting site by ID from DAO: " + ex);
        }
        return siteDto;
    }

    /**
     * Returns all the sites in the database
     *
     * @return list of sites in form of data transfer object
     */
    public List<SiteDto> getListOfSites(){
        List<Site> sites = new ArrayList<>();
        List<SiteDto> siteDtos = new ArrayList<>();
        try {
            sites = siteDao.getSites();
            for (Site site : sites) {
                siteDtos.add(populateDto(site));
            }
        }catch (Exception ex){
            System.out.println("Error getting list of sites: " + ex);
        }
        return siteDtos;
    }

    /**
     * Returns a list of sites by zipCode
     *
     * @param zipCode
     * @return List of sites in form of data transfer object
     */
    public List<SiteDto> getListOfSitesByZip(String zipCode){
        List<Site> sites = new ArrayList<>();
        List<SiteDto> siteDtos = new ArrayList<>();
        try {
            sites = siteDao.getSitesByZipCode(zipCode);
            for (Site site : sites) {
                siteDtos.add(populateDto(site));
            }
        }catch (Exception ex){
            System.out.println("Error getting list of sites by zipCode: " + ex);
        }
        return siteDtos;
    }


    /**
     * populates data transfer object with the fields returned from the dao.
     * @param site
     * @return siteDTO
     */
    protected SiteDto populateDto(Site site){
        SiteDto siteDto = new SiteDto();
        siteDto.setName(site.getName());
        siteDto.setAddress(site.getAddress());
        siteDto.setCity(site.getCity());
        siteDto.setId(site.getId());
        siteDto.setState(site.getState());
        siteDto.setZipCode(site.getZipcode());
        siteDto.setTotal_size(findTotalSize(site));
        siteDto.setPrimaryUse(primaryUseType(site));
        return siteDto;
    }

    /**
     * Finds the total size of the site.
     *
     * @param site to calculate
     * @return total size of the site
     */
    protected long findTotalSize(Site site){
        List<SiteUses> siteUsesList = site.getSiteUses();
        long size = 0;
        for(SiteUses siteUses : siteUsesList){
            size = size + siteUses.getSizeSqft();
        }
        return size;
    }


    /**
     * Iterates through the siteUses and returns the useType that's
     * used the most. Uses a hashmap to find duplicate useTypes using an
     * UseTypes ID as the key.
     *
     * @param site site being checked
     * @return UseType that is used the most.
     */
    protected UseTypes primaryUseType(Site site){
        UseTypes useTypeMax = new UseTypes();
        Map<UseTypes, Long> useTypeMap = new HashMap<>();
        long maxSize = 0;
        for(SiteUses siteUses : site.getSiteUses()){
            if(useTypeMap.containsKey(siteUses.getUseTypes())){
                long count = useTypeMap.get(siteUses.getUseTypes());
                useTypeMap.put(siteUses.getUseTypes(), siteUses.getSizeSqft() + count);
            }else{
                useTypeMap.put(siteUses.getUseTypes(), siteUses.getSizeSqft());
            }
            if(useTypeMap.get(siteUses.getUseTypes()) > maxSize){
                System.out.println(useTypeMap.get(siteUses.getUseTypes()));
                useTypeMax = siteUses.getUseTypes();
                maxSize = useTypeMap.get(siteUses.getUseTypes());
            }
            System.out.println(useTypeMap.toString());
        }
        return useTypeMax;
    }
}
