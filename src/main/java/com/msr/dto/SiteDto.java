package com.msr.dto;

import com.msr.model.UseTypes;

public class SiteDto {

    private int id;
    private String name;
    private String address;
    private String city;
    private String state;
    private String zipCode;
    private long total_size;
    private UseTypes primaryUse;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public long getTotal_size() {
        return total_size;
    }

    public void setTotal_size(long total_size) {
        this.total_size = total_size;
    }

    public UseTypes getPrimaryUse() {
        return primaryUse;
    }

    public void setPrimaryUse(UseTypes primaryUse) {
        this.primaryUse = primaryUse;
    }
}
