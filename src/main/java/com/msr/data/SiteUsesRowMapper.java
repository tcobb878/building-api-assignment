package com.msr.data;

import com.msr.model.Site;
import com.msr.model.SiteUses;
import com.msr.model.UseTypes;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SiteUsesRowMapper implements RowMapper<SiteUses> {

    @Override
    public SiteUses mapRow(ResultSet rs, int rowNum) throws SQLException {
        SiteUses siteUses = new SiteUses();
        UseTypes useTypes = new UseTypes();
        siteUses.setId(rs.getInt("id"));
        siteUses.setDescription(rs.getString("description"));
        siteUses.setSiteId(rs.getInt("site_id"));
        siteUses.setSizeSqft(rs.getLong("size_sqft"));
        siteUses.setUseTypeId(rs.getInt("use_type_id"));
        useTypes.setId(rs.getInt("use_type_id"));
        useTypes.setName(rs.getString("name"));
        siteUses.setUseTypes(useTypes);
        return siteUses;
    }
}
