package com.msr.data;


import com.msr.model.Site;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SiteRowMapper implements RowMapper<Site> {

    @Override
    public Site mapRow(ResultSet rs, int rowNum) throws SQLException{
        Site site = new Site();
        site.setId(rs.getInt("id"));
        site.setAddress(rs.getString("address"));
        site.setState(rs.getString("state"));
        site.setCity(rs.getString("city"));
        site.setName(rs.getString("name"));
        site.setZipcode(rs.getString("zipcode"));
        return site;
    }
}
