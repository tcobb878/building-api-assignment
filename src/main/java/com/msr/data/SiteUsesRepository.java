package com.msr.data;

import com.msr.model.SiteUses;
import org.springframework.data.repository.CrudRepository;

public interface SiteUsesRepository extends CrudRepository<SiteUses, Integer> {

}
