package com.msr.data;

import com.msr.model.Site;
import com.msr.model.SiteUses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Global DAO functionality not domain driven
 *
 * Would normally error handle these better other than printing out exception.
 *
 * @author Measurabl
 * @since 2019-06-06
 */
@Service
@Slf4j
public class SiteDao {
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public SiteDao(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public Site findSiteById(int id) {
        final String GET_SITE = "SELECT \n" +
                "site.ID,\n" +
                "site.NAME,\n" +
                "site.ADDRESS,\n" +
                "site.CITY,\n" +
                "site.STATE,\n" +
                "site.ZIPCODE\n" +
                " FROM SITE site " +
                "WHERE site.ID = :id";

        SqlParameterSource parameters = new MapSqlParameterSource().addValue("id", id);
        List<Site> sites = new ArrayList<>();
        try {
            sites = namedParameterJdbcTemplate.query(GET_SITE, parameters, new SiteRowMapper());
        } catch (Exception ex) {
            System.out.print(ex);
        }
        //Should iterate through list double check not more than one result is returned
        sites.get(0).setSiteUses(findSiteUsesByID(id));

        return sites.get(0);
    }

    public List<Site> getSites() {
        final String GET_SITES = "SELECT \n" +
                "site.ID,\n" +
                "site.NAME,\n" +
                "site.ADDRESS,\n" +
                "site.CITY,\n" +
                "site.STATE,\n" +
                "site.ZIPCODE\n" +
                " FROM SITE site ";

        List<Site> sites = new ArrayList<>();
        sites = namedParameterJdbcTemplate.query(GET_SITES, new SiteRowMapper());

        for(int x = 0; x < sites.size(); x++) {
            sites.get(x).setSiteUses(findSiteUsesByID(sites.get(x).getId()));
        }
        return sites;
    }

    public List<Site> getSitesByZipCode(String zipCode){
        final String GET_SITES_ZIP_CODE = "SELECT \n" +
                "site.ID,\n" +
                "site.NAME,\n" +
                "site.ADDRESS,\n" +
                "site.CITY,\n" +
                "site.STATE,\n" +
                "site.ZIPCODE\n" +
                " FROM SITE site " +
                "WHERE site.ZIPCODE = :zipCode";

        SqlParameterSource parameters = new MapSqlParameterSource().addValue("zipCode", zipCode);
        List<Site> sites = new ArrayList<>();
        sites = namedParameterJdbcTemplate.query(GET_SITES_ZIP_CODE, parameters, new SiteRowMapper());

        for(int x = 0; x < sites.size(); x++) {
            sites.get(x).setSiteUses(findSiteUsesByID(sites.get(x).getId()));
        }
        return sites;
    }

    public List<SiteUses> findSiteUsesByID(int id) {
        final String GET_SITE_USE = "SELECT \n" +
                "site_uses.id,\n" +
                "site_uses.description,\n" +
                "site_uses.site_id,\n" +
                "site_uses.size_sqft,\n" +
                "site_uses.use_type_id,\n" +
                "use_types.name\n" +
                "FROM SITE_USES site_uses\n" +
                "JOIN USE_TYPES use_types ON use_types.id = site_uses.use_type_id\n" +
                "WHERE site_id = :id";

        SqlParameterSource parameters = new MapSqlParameterSource().addValue("id", id);
        Map<String, String> param = new HashMap<>();
        param.put("id", Integer.toString(id));
        List<SiteUses> siteUses = new ArrayList<>();
        try {
            siteUses = namedParameterJdbcTemplate.query(GET_SITE_USE, parameters, new SiteUsesRowMapper());
        } catch (Exception ex) {
            System.out.print(ex);
        }
        return siteUses;
    }

}


////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
